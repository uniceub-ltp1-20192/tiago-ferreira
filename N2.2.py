#introduz a quantidade de cebolas na variavel "cebola"
cebolas = 300
#introduz na variavel "cebolas_na_caixa" um numero
cebolas_na_caixa = 120
#introduz na variavel "espaco_caixa" um numero
espaco_caixa = 5
#introduz na variavel "caixas" um numero
caixas = 60
#introduz na variavel "cebolas_fora_da_caixa" uma conta de subtracao
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)

caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print()
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print()
print("Em cada caixa cabem", espaco_caixa, "cebolas")
print()
print("Ainda temos", caixas_vazias, "caixas vazias")
print()
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
